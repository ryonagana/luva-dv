#include <Ultrasonic.h>
#include <SoftwareServo.h>


const int MAX_SENSORS  = 2;
const int sensorInputTrigger[MAX_SENSORS] = {8, 12};
const int sensorInputEcho[MAX_SENSORS] = {7, 11};
const int servoPin[MAX_SENSORS] = {3, 2};


#define SENSOR0  0
#define SENSOR1  1
#define SERVO0     0
#define SERVO1     1

#define MAX_DIST 3000
#define MIN_DIST 5

Ultrasonic sensors[MAX_SENSORS] = {
        Ultrasonic (sensorInputTrigger[0], sensorInputEcho[0]),
        Ultrasonic (sensorInputTrigger[1], sensorInputEcho[1]),
};

SoftwareServo servoList[MAX_SENSORS];

int servoAngles[MAX_SENSORS];

bool refresh = false;


void init_servos(){
    int i;
    for(i = 0; i < MAX_SENSORS; i++){
        servoList[i].attach(servoPin[i]);
        delay(10);
        servoList[i].write(0);
        delay(500);
    }
}


long getSensorTiming(int sensor_id){
    return sensors[sensor_id].timing();
}

float sensorDistance(long timing,  int sensor_id){
      return sensors[sensor_id].convert(timing, Ultrasonic::CM);
}



void setup() {
  // put your setup code here, to run once:
  //Serial.begin(9600);
  init_servos();


}

void loop() {
  // put your main code here, to run repeatedly:
    long sensLeft, sensRight;
    float distLeft, distRight;

    int angleLeft, angleRight;



    sensLeft = getSensorTiming(SENSOR0);
    sensRight = getSensorTiming(SENSOR1);

    angleRight = servoList[0].read();
    angleLeft    = servoList[1].read();

    distLeft = sensorDistance(sensLeft, SENSOR0);
    distRight = sensorDistance(sensRight, SENSOR1);

    servoAngles[0] =  map(distLeft, MIN_DIST, 120,0,179);
    servoAngles[1] =  map(distRight, MIN_DIST, 120,0,179);

    servoAngles[0] = constrain(servoAngles[0], 0, 179);
    servoAngles[1] = constrain(servoAngles[1], 0, 179);


    if(distLeft < MIN_DIST || distLeft > MAX_DIST){
        servoList[0].write(0);
  
    }else {
         servoList[0].write(servoAngles[0]);
 
    }

    if(distRight < MIN_DIST || distRight > MAX_DIST){
        servoList[1].write(0);

    }else {
         servoList[1].write(servoAngles[1]);
 
    }

  
    SoftwareServo::refresh();
      delay(50);



    /*
    Serial.print("SENSOR DIR: ");
    Serial.print(distRight);
   Serial.print("ANG: ");
   Serial.println(servoAngles[1]);
   */






  //long m = u.timing();
  //float cm = u.convert(m, Ultrasonic::CM);
 // Serial.println(cm);





}
