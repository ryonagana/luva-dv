import os
import sys
import platform
SPEED_OF_SOUND = 58.3

def clr():
    os.system("clear")

"""
passa o valor de tempo de resposta o sinal do componente  ultrassonico
e converte o tempo em distancia em centímetro(cm)
"""
def calc_distancia(duration, maxdist, mindist):
    dist = duration /  58.3

    if( dist > maxdist ):
         dist = maxdist


    return dist


if __name__ == "__main__":

    clr()
    max_dist = 450
    min_dist = 100
    duration = 5350
    distancia = calc_distancia(duration, max_dist, min_dist )
    print ("DISTANCIA EM CM: {0}".format(distancia))
