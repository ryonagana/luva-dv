#include <SoftwareServo.h>
#include <NewPing.h>

const int MAX_SENSORS = 2;
const int MIN_DISTANCE = 0;
const int MAX_DISTANCE = 500;

const int DistanceFar = 380;
const int DistanceNear = 5;

// FILTER
const int READINGS_BUFFER_SIZE = 15;
unsigned long readingArray[2][READINGS_BUFFER_SIZE];
unsigned long readingSum[2] = {0, 0};
int readingIndex[2] = {0, 0};

const int sensorInputTrigger[MAX_SENSORS] = {8,12};
const int sensorInputEcho[MAX_SENSORS] = {7,11};
const int servoPin[MAX_SENSORS] = {3, 2};
const int maxReadings = 5;

NewPing sensorList[MAX_SENSORS] = {
  NewPing(sensorInputTrigger[0], sensorInputEcho[0], MAX_DISTANCE),
  NewPing(sensorInputTrigger[1], sensorInputEcho[1], MAX_DISTANCE),
};


SoftwareServo servoList[MAX_SENSORS];
unsigned long servoAngles[MAX_SENSORS];

unsigned int getSensorPing(int sensor){
  unsigned int ping = sensorList[sensor].ping();
  return sensorList[sensor].convert_cm(ping);
}

void setup(){
  Serial.begin(115200);

  for( int i = 0; i < MAX_SENSORS; i++){
    servoList[i].attach( servoPin[i]);
    servoList[i].write(0);
  }
}

unsigned long getAverageDistance(const unsigned long reading, int sensorIndex){


  readingSum[sensorIndex] -= readingArray[sensorIndex][readingIndex[sensorIndex]];
  readingArray[sensorIndex][readingIndex[sensorIndex]] = reading;
  readingSum[sensorIndex] += readingArray[sensorIndex][readingIndex[sensorIndex]];
  readingIndex[sensorIndex]++;

  if (readingIndex[sensorIndex] >= READINGS_BUFFER_SIZE) readingIndex[sensorIndex] = 0;

  return readingSum[sensorIndex]/READINGS_BUFFER_SIZE;


}

void loop() {
  unsigned long last_distance_right, last_distance_left;
  unsigned long sensor0 =  getSensorPing(0);
  unsigned long sensor1 = getSensorPing(1);



  last_distance_right = getAverageDistance(sensor0, 0);
  last_distance_left = getAverageDistance(sensor1, 1);

  servoAngles[0] = map(last_distance_right, DistanceNear, 120, 0, 179);
  servoAngles[1] = map(last_distance_left, DistanceNear, 120, 0, 179);

  servoAngles[0] = constrain(servoAngles[0], 0, 179);
  servoAngles[1] = constrain(servoAngles[1], 0, 179);


  /*
  Serial.print("SENSOR0:  ");
  Serial.print(sensor0);
  Serial.print(", ");
  Serial.print("SENSOR1: ");
  Serial.print(sensor1);
  Serial.print(", ");
  Serial.print("DIST LEFT");
  Serial.print(last_distance_right);
  Serial.print(", ");
  Serial.print("DIST RIGHT ");
  Serial.println(last_distance_left);
  */

    servoList[0].write(servoAngles[0]);
    servoList[1].write(servoAngles[1]);



  /*
  if (servoAngles[0] > DistanceNear) {
    servoList[0].write(servoAngles[0]);
  } else {
    servoList[0].write(0);
  }

  if (servoAngles[1] > DistanceNear) {
    servoList[1].write(servoAngles[1]);
  } else {
    servoList[1].write(0);
  }
  */

  SoftwareServo::refresh();

  delay(50);

}
