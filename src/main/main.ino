#include <SoftwareServo.h>
#include <Ultrasonic.h>

const int MAX_SENSORS = 2;
const int MIN_DISTANCE = 0;
const int MAX_DISTANCE = 500;

const int DistanceFar = 380;
const int DistanceNear = 5;

// FILTER
const int READINGS_BUFFER_SIZE = 15;
unsigned long readingArray[2][READINGS_BUFFER_SIZE];
unsigned long readingSum[2] = {0, 0};
int readingIndex[2] = {0, 0};

const int sensorInputTrigger[MAX_SENSORS] = {8, 12};
const int sensorInputEcho[MAX_SENSORS] = {7, 11};
const int servoPin[MAX_SENSORS] = {3, 2};
const int maxReadings = 5;

Ultrasonic sensorList[MAX_SENSORS] = {
  Ultrasonic (sensorInputTrigger[0], sensorInputEcho[0]),
  Ultrasonic (sensorInputTrigger[1], sensorInputEcho[1]),
};


SoftwareServo servoList[MAX_SENSORS];
unsigned long servoAngles[MAX_SENSORS];

float getSensorPing(int sensor) {
  long msec = sensorList[sensor].timing();
  return sensorList[sensor].convert(msec, Ultrasonic::CM);
}

void setup() {
  Serial.begin(9600);

  for ( int i = 0; i < MAX_SENSORS; i++) {
    servoList[i].attach( servoPin[i]);
    servoList[i].write(0);
  }
}

unsigned long getAverageDistance(const unsigned long reading, int sensorIndex) {


  readingSum[sensorIndex] -= readingArray[sensorIndex][readingIndex[sensorIndex]];
  readingArray[sensorIndex][readingIndex[sensorIndex]] = reading;
  readingSum[sensorIndex] += readingArray[sensorIndex][readingIndex[sensorIndex]];
  readingIndex[sensorIndex]++;

  if (readingIndex[sensorIndex] >= READINGS_BUFFER_SIZE) readingIndex[sensorIndex] = 0;

  return readingSum[sensorIndex] / READINGS_BUFFER_SIZE;


}

void loop() {
  long msec0 = sensorList[0].timing();
  long msec1 = sensorList[1].timing();

  Serial.print(msec0);
  Serial.print(msec1);

  //SoftwareServo::refresh();

  delay(1000);

}
